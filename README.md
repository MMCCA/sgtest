## Example Output
For the main program:

java -jar counter.jar 1900 2000 test.txt

1951


For the data set generator program:

java -jar generator.jar 1900 2000 1000 test.txt

1935

## Example Dataset
See src/test/resources
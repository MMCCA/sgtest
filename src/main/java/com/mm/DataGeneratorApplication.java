package com.mm;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * This is a generator class used to create test data. It uses a very simple algorithm to determine what the year with
 * the most number of people is. It will write to a file a series of date ranges and output to the console the year
 * with the most number of living people.
 *
 * Takes 4 arguments:
 * START YEAR
 * END YEAR
 * NUM TO GENERATE
 * OUTPUT LOCATION
 */
public class DataGeneratorApplication {
    public static void main(String[] args) {
        validateArgsCount(args);

        int start = Integer.parseInt(args[0]);
        int end = Integer.parseInt(args[1]);
        int range = end - start;
        int count = Integer.parseInt(args[2]);
        String outFile = args[3];

        int[] living = new int[range];
        generateFile(start, range, count, outFile, living);

        int pos = findMax(living);
        System.out.println(start + pos);
    }

    /**
     * Generates a file of dates of the format START END. It fills the array living with the total number of people
     * alive per year. The indices of the array are considered offsets to the start year.
     *
     * @param start   The year to begin generating dates from
     * @param years   Maximum years after start that someone can be alive
     * @param count   Number of entries to generate
     * @param outFile Where to store results
     * @param living  Array to keep track of how many people are alive for a specific year
     */
    private static void generateFile(int start, int years, int count, String outFile, int[] living) {
        try {
            PrintWriter writer = new PrintWriter(outFile);

            Random rand = new Random();
            for (int i = 0; i < count; i++) {
                int upper = 1 + rand.nextInt(years);
                int lower = rand.nextInt(upper);
                for (int j = lower; j < upper; j++) {
                    living[j]++;
                }
                writer.println((start + lower) + " " + (start + upper));
            }

            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Finds the index with the highest count
     *
     * @param living An array where each index is the total number of people alive
     */
    private static int findMax(int[] living) {
        int max = 0;
        int pos = 0;
        for (int i = 0; i < living.length; i++) {
            if (living[i] >= max) {
                max = living[i];
                pos = i;
            }
        }
        return pos;
    }

    private static void validateArgsCount(String[] args) {
        if (args.length != 4) {
            System.out.println("Incorrect number of arguments!");
            System.out.println("java -jar generator.jar START END COUNT FILE");
            System.exit(1);
        }
    }
}

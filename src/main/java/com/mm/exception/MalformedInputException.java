package com.mm.exception;

/**
 * Denotes input was not in the expected format
 */
public class MalformedInputException extends Exception {
    public MalformedInputException(String message) {
        super(message);
    }
}

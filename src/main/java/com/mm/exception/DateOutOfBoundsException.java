package com.mm.exception;

/**
 * Denotes that a given date was not within an allowed range
 */
public class DateOutOfBoundsException extends Exception {
    public DateOutOfBoundsException(String message) {
        super(message);
    }
}

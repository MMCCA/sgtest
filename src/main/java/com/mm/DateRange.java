package com.mm;

/**
 * A simple class that holds two numeric values that represent the start and end years on a date interval.
 */
public class DateRange {
    private int start;
    private int end;

    public DateRange(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }
}

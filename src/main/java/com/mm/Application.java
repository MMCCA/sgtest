package com.mm;

import java.io.FileInputStream;

public class Application {
    public static void main(String[] args) {
        validateArgsCount(args);

        try {
            int start = Integer.parseInt(args[0]);
            int end = Integer.parseInt(args[1]);
            FileInputStream fStream = new FileInputStream(args[2]);

            Counter counter = new Counter(start, end);
            int year = counter.getMaxLiving(fStream);
            System.out.println(year);

            fStream.close();
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void validateArgsCount(String[] args) {
        if (args.length != 3) {
            System.out.println("ERROR: Incorrect number of arguments!");
            System.out.println("java -jar counter.jar START_DATE END_DATE FILE_LOCATION");
            System.exit(1);
        }
    }
}

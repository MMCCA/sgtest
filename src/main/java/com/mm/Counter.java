package com.mm;

import com.mm.exception.DateOutOfBoundsException;
import com.mm.exception.MalformedInputException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * The Counter class is used to find the maximum number of people alive within a date range.
 */
public class Counter {
    private int maxYear;
    private int minYear;

    public Counter(int minYear, int maxYear) {
        this.minYear = minYear;
        this.maxYear = maxYear;
    }

    /**
     * Returns the year with the most number of living people. Years are expected to be in a format of START END, where
     * minYear <= START & END <= maxYear. It is also expected that START < END. If there are multiple years with the same
     * number of people alive, there is no guarantee which year will be returned. In the current implementation, the most recent
     * year will be returned.
     *
     * @param iStream a stream for reading in birth/death dates
     * @return The year with the maximum number of people alive
     * @throws IOException              if there is an issue reading in the stream
     * @throws MalformedInputException  if the input is not in the specified format
     * @throws DateOutOfBoundsException if there is a date in the input outside of the counter's range
     */
    public int getMaxLiving(InputStream iStream) throws IOException, MalformedInputException, DateOutOfBoundsException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
        int[] livelinessArray = buildLivelinessCount(reader);
        int maxPos = getMaxPos(livelinessArray);
        return minYear + maxPos;
    }

    /**
     * Builds out an array where each position is the change in number of living people for that year.
     * For instance, a value of 0 would mean there was no change in the number of people alive. A value of 1 would mean
     * there was one more person alive than the previous year, and a value of -1 would mean there was one less person alive
     * compared to the previous year. The array indices are offsets from the counter's minYear.
     *
     * @param reader Input for reading in the dates
     * @return Array containing the deltas of each year
     * @throws IOException              if there is an issue reading the input
     * @throws DateOutOfBoundsException if there is a date in the input that is outside of the counter's min/max year
     */
    protected int[] buildLivelinessCount(BufferedReader reader) throws IOException, DateOutOfBoundsException, MalformedInputException {
        String line;
        int[] livelinessArray = new int[1 + maxYear - minYear];
        while ((line = reader.readLine()) != null) {
            DateRange range = parseDateRow(line);

            if (range.getStart() < minYear || range.getEnd() > maxYear)
                throw new DateOutOfBoundsException("The date range: " + line + " was outside of the counter's range: " + minYear + " " + maxYear);

            livelinessArray[range.getStart() - minYear]++;
            livelinessArray[range.getEnd() - minYear]--;
        }
        return livelinessArray;
    }

    /**
     * Iterates through an array of delta's and finds the position that has maximum sum.
     *
     * @param livelinessArray An array containing incremental changes
     * @return The position in the array with the maximum sum
     */
    protected int getMaxPos(int[] livelinessArray) {
        int max = 0;
        int pos = 0;
        int total = 0;

        for (int i = 0; i < livelinessArray.length; i++) {

            total += livelinessArray[i];

            if (total >= max) {
                max = total;
                pos = i;
            }
        }
        return pos;
    }

    /**
     * Parses a line of text of the format:
     * START END
     * into a DateRange object. It expects start and end to be numeric values
     *
     * @param row A string representing a date range: "START END"
     * @return A DateRange object initialized to the input
     * @throws MalformedInputException if the input given is not of the specified format
     */
    protected DateRange parseDateRow(String row) throws MalformedInputException {
        String[] dates = row.split("\\s+");

        if (dates.length != 2)
            throw new MalformedInputException("Input row had less than two date fields:\n" + row);

        int start;
        int end;

        try {
            start = Integer.parseInt(dates[0]);
            end = Integer.parseInt(dates[1]);
        } catch (NumberFormatException e) {
            throw new MalformedInputException("The input dates were not of the type int:\n" + row);
        }

        return new DateRange(start, end);
    }
}

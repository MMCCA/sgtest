package com.mm;

import com.mm.exception.MalformedInputException;
import org.junit.Test;

public class ParseTest {
    @Test
    public void testParseDateRange() throws MalformedInputException {
        Counter counter = new Counter(1900, 2000);
        DateRange range = counter.parseDateRow("1950 1975");
        assert (range.getStart() == 1950);
        assert (range.getEnd() == 1975);
    }

    @Test(expected = MalformedInputException.class)
    public void testMissingDateRange() throws MalformedInputException {
        Counter counter = new Counter(1900, 2000);
        DateRange range = counter.parseDateRow("1950");
    }

    @Test(expected = MalformedInputException.class)
    public void testNonDateRange() throws MalformedInputException {
        Counter counter = new Counter(1900, 2000);
        DateRange range = counter.parseDateRow("1950 cheese");
    }
}

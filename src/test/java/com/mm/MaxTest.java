package com.mm;

import org.junit.Test;

public class MaxTest {

    @Test
    public void testZeroes() {
        int[] arr = new int[10];
        Counter counter = new Counter(1900, 2000);
        int max = counter.getMaxPos(arr);
        assert (max == 9);
    }

    @Test
    public void testOnePerson() {
        int[] arr = new int[]{0, 1, 0, 0, -1, 0};
        Counter counter = new Counter(1900, 2000);
        int max = counter.getMaxPos(arr);
        assert (max == 3);
    }

    @Test
    public void overlappingRange() {
        int[] arr = new int[]{0, 1, 0, 1, 0, -1, 0, -1};
        Counter counter = new Counter(1900, 2000);
        int max = counter.getMaxPos(arr);
        assert (max == 4);
    }

    @Test
    public void testMultipleRange() {
        int[] arr = new int[]{0, 1, 0, 2, -1, 0, 1, -2, 0, 1, 0, -1, -1, 0};
        Counter counter = new Counter(1900, 2000);
        int max = counter.getMaxPos(arr);
        assert (max == 6);
    }
}

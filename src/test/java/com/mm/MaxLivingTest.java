package com.mm;

import com.mm.exception.DateOutOfBoundsException;
import com.mm.exception.MalformedInputException;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Tallath on 3/7/2017.
 */
public class MaxLivingTest {
    @Test
    public void testNoPeople() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        int date = counter.getMaxLiving(iStream);
        assert (date == 2000);
    }

    @Test
    public void testOnePerson() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "1940 1951";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        int date = counter.getMaxLiving(iStream);
        assert (date == 1950);
    }

    @Test
    public void testTwoIndependent() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "1940 1951\n" +
                "1960 1971";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        int date = counter.getMaxLiving(iStream);
        assert (date == 1970);
    }

    @Test
    public void testMultiple() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "1940 1951\n" +
                "1960 1971\n" +
                "1945 1965";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        int date = counter.getMaxLiving(iStream);
        assert (date == 1964);
    }

    @Test(expected = DateOutOfBoundsException.class)
    public void testDateOutOfBounds() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "1940 1951\n" +
                "1960 1971\n" +
                "1945 2050";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        counter.getMaxLiving(iStream);
    }

    @Test(expected = MalformedInputException.class)
    public void testMissingDate() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "1940 1951\n" +
                "1960 1971\n" +
                "1945";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        counter.getMaxLiving(iStream);
    }

    @Test(expected = MalformedInputException.class)
    public void testNonDate() throws IOException, MalformedInputException, DateOutOfBoundsException {
        String dates = "1940 1951\n" +
                "1960 1971\n" +
                "1945 c";
        InputStream iStream = new ByteArrayInputStream(dates.getBytes());
        Counter counter = new Counter(1900, 2000);
        counter.getMaxLiving(iStream);
    }
}
